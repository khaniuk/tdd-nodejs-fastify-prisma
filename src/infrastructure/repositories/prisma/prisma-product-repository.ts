import { prisma } from "@/lib/prisma";
import { Prisma } from "@prisma/client";

import { ProductRepository } from "@/domain/ports/driven/product-repository";

export class PrismaProductRepository implements ProductRepository {
  async findAll() {
    const products = await prisma.product.findMany();

    return products;
  }

  async findById(id: string) {
    const product = await prisma.product.findUnique({
      where: {
        id,
      },
    });

    return product;
  }

  async create(data: Prisma.ProductCreateInput) {
    const product = await prisma.product.create({
      data,
    });

    return product;
  }

  async update(id: string, data: Prisma.ProductUpdateInput) {
    const productData = this.getProductData(data);

    const product = await prisma.product.update({
      where: {
        id: id,
      },
      data: {
        ...productData,
      },
    });

    return product;
  }

  private getProductData(
    product: Prisma.ProductUpdateInput
  ): Prisma.ProductUpdateInput {
    let productDta: Prisma.ProductUpdateInput = {
      name: product.name,
      price: product.price,
    };

    return productDta;
  }

  async delete(id: string) {
    await prisma.product.delete({
      where: {
        id: id,
      },
    });
  }
}
