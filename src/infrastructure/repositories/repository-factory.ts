import { PrismaProductRepository } from "./prisma/prisma-product-repository";

export class RepositoryFactory {
  public static productRepository() {
    const productRepository = new PrismaProductRepository();

    return productRepository;
  }
}
