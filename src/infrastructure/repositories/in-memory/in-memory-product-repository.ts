import { ProductRepository } from "@/domain/ports/driven/product-repository";
import { Prisma, Product } from "@prisma/client";
import { randomUUID } from "node:crypto";

export class InMemoryProductRepository implements ProductRepository {
  public products: Product[] = [];

  async findAll() {
    const roles = this.products;

    return roles;
  }

  async findById(id: string) {
    const product = this.products.find((item) => item.id === id);

    if (!product) {
      return null;
    }

    return product;
  }

  async create(data: Prisma.ProductCreateInput) {
    const product: Product = {
      id: randomUUID(),
      name: data.name,
      price: new Prisma.Decimal(data.price.toString()),
    };

    this.products.push(product);

    return product;
  }

  async update(id: string, data: Prisma.ProductCreateInput) {
    const findIndex = this.products.findIndex((product) => product.id == id);

    if (findIndex == -1) {
      throw Error("Product not found");
    }

    this.products[findIndex].name = data.name;
    this.products[findIndex].price = new Prisma.Decimal(data.price.toString());

    return this.products[findIndex];
  }

  async delete(id: string) {
    const findIndex = this.products.findIndex((product) => product.id == id);

    if (findIndex != -1) {
      this.products.splice(findIndex, 1);
    }
  }
}
