import { app } from "@/app";
import request from "supertest";
import { afterAll, beforeAll, describe, expect, it } from "vitest";

describe("CRUD Product (e2e)", () => {
  beforeAll(async () => {
    await app.ready();
  });

  afterAll(async () => {
    await app.close();
  });

  it("should be able to crud", async () => {
    // Create product
    const responseCreateProduct = await request(app.server)
      .post("/products")
      .send({
        name: "Asus",
        price: 5900.5,
      });

    const productId: string = responseCreateProduct.body.id;

    expect(responseCreateProduct.statusCode).toEqual(201);

    // Update product
    const responseUpdateProduct = await request(app.server)
      .put(`/products/${productId}`)
      .send({
        name: "Asus Rog",
        price: 8500.5,
      });

    expect(responseUpdateProduct.statusCode).toEqual(200);

    // Get products
    const responseGetProducts = await request(app.server)
      .get("/products")
      .send();

    expect(responseGetProducts.statusCode).toEqual(200);

    // Get product by id
    const responseGetProductById = await request(app.server).get(
      `/products/${productId}`
    );

    expect(responseGetProductById.statusCode).toEqual(200);

    // Delete product
    const responseDeleteProduct = await request(app.server).delete(
      `/products/${productId}`
    );

    expect(responseDeleteProduct.statusCode).toEqual(401);
  });
});
