import { FastifyReply, FastifyRequest } from "fastify";
import { z } from "zod";

import { ApplicationFactory } from "@/application/application-factory";
import { ProductDto } from "@/domain/models/product-dto";

export async function getProducts(
  request: FastifyRequest,
  reply: FastifyReply
) {
  let products: Array<ProductDto>;

  try {
    const productService = ApplicationFactory.productService();
    products = await productService.getAll();
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(200).send(products);
}

export async function getProductById(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const productParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = productParameterSchema.parse(request.params);

  let product: ProductDto;

  try {
    const productService = ApplicationFactory.productService();
    product = await productService.getById(id);
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(200).send(product);
}

export async function createProduct(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const createProductBodySchema = z.object({
    name: z.string(),
    price: z.number(),
  });

  const { name, price } = createProductBodySchema.parse(request.body);

  let product: ProductDto;

  try {
    const productService = ApplicationFactory.productService();
    product = await productService.create({
      id: "",
      name,
      price,
    });
  } catch (err) {
    if (err instanceof Error) {
      return reply.status(409).send({ message: err.message });
    }

    throw err;
  }

  return reply.status(201).send(product);
}

export async function updateProduct(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const productParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = productParameterSchema.parse(request.params);

  const updateProductBodySchema = z.object({
    name: z.string(),
    price: z.number(),
  });

  const { name, price } = updateProductBodySchema.parse(request.body);

  try {
    const productService = ApplicationFactory.productService();
    await productService.update({
      id,
      name,
      price,
    });
  } catch (err) {
    if (err instanceof Error) {
      return reply.status(409).send({ message: err.message });
    }

    throw err;
  }

  return reply.status(200).send();
}

export async function deleteProduct(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const productParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = productParameterSchema.parse(request.params);

  try {
    const productService = ApplicationFactory.productService();
    await productService.delete(id);
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(401).send();
}
