import { FastifyInstance } from "fastify";

import {
  createProduct,
  deleteProduct,
  getProductById,
  getProducts,
  updateProduct,
} from "./controllers/product-controller";

export async function appRoute(app: FastifyInstance) {
  app.get("/products", getProducts);
  app.get("/products/:id", getProductById);
  app.post("/products", createProduct);
  app.put("/products/:id", updateProduct);
  app.delete("/products/:id", deleteProduct);
}
