import { InMemoryProductRepository } from "@/infrastructure/repositories/in-memory/in-memory-product-repository";
import { randomUUID } from "crypto";
import { beforeEach, describe, expect, it } from "vitest";
import { ResourceNotFoundError } from "../errors/resource-not-found-error";
import { ProductService } from "./product-service";

let productRepository: InMemoryProductRepository;
let productService: ProductService;

describe("Product services", () => {
  beforeEach(() => {
    productRepository = new InMemoryProductRepository();
    productService = new ProductService(productRepository);
  });

  it("should to get all products", async () => {
    await productService.create({
      id: randomUUID(),
      name: "Asus",
      price: 10000.5,
    });

    await productService.create({
      id: randomUUID(),
      name: "Acer",
      price: 9800.0,
    });

    const products = await productService.getAll();

    expect(products).toHaveLength(2);
  });

  it("should to get product", async () => {
    const newProduct = await productService.create({
      id: randomUUID(),
      name: "Asus",
      price: 10000.5,
    });

    const id: string = String(newProduct.id);

    const product = await productService.getById(id);

    expect(product.name).toEqual("Asus");
  });

  it("should to create product", async () => {
    const product = await productService.create({
      id: randomUUID(),
      name: "Asus Rog",
      price: 9900.5,
    });

    expect(product.id).toEqual(expect.any(String));
  });

  it("should to update product", async () => {
    const newProduct = await await productService.create({
      id: randomUUID(),
      name: "Asus",
      price: 9900.5,
    });

    const role = await productService.update({
      id: newProduct.id,
      name: "Asus Rog",
      price: 10000.5,
    });

    expect(role.name).toEqual("Asus Rog");
  });

  it("should to delete product", async () => {
    const newProduct = await productService.create({
      id: randomUUID(),
      name: "Asus",
      price: 9900.5,
    });

    const id: string = String(newProduct.id);

    await productService.delete(id);

    try {
      const product = await productService.getById(id);

      expect(product.id).toEqual(id);
    } catch (error) {
      if (error instanceof ResourceNotFoundError) {
        console.error(error.message);
        expect(error.message).toEqual("Resource not found");
      }
    }
  });
});
