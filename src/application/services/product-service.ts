import { ProductDto } from "@/domain/models/product-dto";
import { ProductRepository } from "@/domain/ports/driven/product-repository";
import { CreateProduct } from "@/domain/ports/driver/create-product";
import { DeleteProduct } from "@/domain/ports/driver/delete-product";
import { GetProduct } from "@/domain/ports/driver/get-product";
import { UpdateProduct } from "@/domain/ports/driver/update-product";
import { ResourceNotFoundError } from "../errors/resource-not-found-error";

export class ProductService
  implements CreateProduct, GetProduct, UpdateProduct, DeleteProduct
{
  constructor(private productRepository: ProductRepository) {}
  async getAll(): Promise<ProductDto[]> {
    let products: Array<ProductDto> = [];

    const productResponse = await this.productRepository.findAll();

    productResponse.forEach((product) => {
      products.push({
        id: product.id,
        name: product.name,
        price: Number(product.price),
      });
    });

    return products;
  }

  async getById(id: string): Promise<ProductDto> {
    const product = await this.productRepository.findById(id);

    if (!product) {
      throw new ResourceNotFoundError();
    }

    return {
      id: product.id,
      name: product.name,
      price: Number(product.price),
    };
  }

  async create({ name, price }: ProductDto): Promise<ProductDto> {
    const product = await this.productRepository.create({
      name,
      price,
    });

    return {
      id: product.id,
      name: product.name,
      price: Number(product.price),
    };
  }

  async update({ id, name, price }: ProductDto): Promise<ProductDto> {
    if (!id) {
      throw new Error("Product identify not valid");
    }

    const productResult = await this.productRepository.findById(id);
    if (!productResult) {
      throw new ResourceNotFoundError();
    }

    const product = await this.productRepository.update(id, {
      name,
      price,
    });

    return {
      id: product.id,
      name: product.name,
      price: Number(product.price),
    };
  }

  async delete(id: string): Promise<void> {
    if (!id) {
      throw new ResourceNotFoundError();
    }

    await this.productRepository.delete(id);
  }
}
