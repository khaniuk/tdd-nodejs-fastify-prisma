import { RepositoryFactory } from "@/infrastructure/repositories/repository-factory";
import { ProductService } from "./services/product-service";

export class ApplicationFactory {
  private static productRepository = RepositoryFactory.productRepository();

  public static productService() {
    const productService = new ProductService(this.productRepository);

    return productService;
  }
}
