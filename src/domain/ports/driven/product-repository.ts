import { Prisma, Product } from "@prisma/client";

export interface ProductRepository {
  findAll(): Promise<Array<Product>>;

  findById(id: string): Promise<Product | null>;

  create(data: Prisma.ProductCreateInput): Promise<Product>;

  update(id: string, data: Prisma.ProductUpdateInput): Promise<Product>;

  delete(id: string): Promise<void>;
}
