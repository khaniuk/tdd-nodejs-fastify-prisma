import { ProductDto } from "@/domain/models/product-dto";

export interface CreateProduct {
  create(data: ProductDto): Promise<ProductDto>;
}
