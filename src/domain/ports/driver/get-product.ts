import { ProductDto } from "@/domain/models/product-dto";

export interface GetProduct {
  getAll(): Promise<Array<ProductDto>>;

  getById(id: string): Promise<ProductDto>;
}
