import { ProductDto } from "@/domain/models/product-dto";

export interface UpdateProduct {
  update(data: ProductDto): Promise<ProductDto>;
}
