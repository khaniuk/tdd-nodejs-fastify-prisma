export class ProductDto {
  constructor(
    readonly id: string | null,
    readonly name: string,
    readonly price: number
  ) {}
}
