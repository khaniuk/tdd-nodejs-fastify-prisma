# TDD Nodejs + Fastify + Prisma

## Description

Example of how to apply Test Driven Development (TDD) in a basic crud.

## Install dependencies

```bash
yarn install
```

## Create Data Base with Docker

```bash
docker componse up -d

# or

docker-componse up -d
```

## Start project

```bash
yarn dev
# or
npm run dev
```

## Execute Tests

Run services test

```bash
yarn test

# or

yarn test:watch
```

Run test End to end

```bash
yarn test:e2e

# or

yarn test:e2e:watch
```

Run test coverage

```bash
yarn test:coverage

# and

yarn test:ui
```
